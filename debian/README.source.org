
** For more information

For the record, [[https://github.com/ugexe/zef/issues/117][zef issue #117 on github]] contains a lot of information
that was used to create this package.

** Some releases have no changelog entries

Due to a mess-up releases 0.8.2-1, 0.8.0-1, 0.7.4-1 and 0.6.2-2 were
committed to this repo:

 https://salsa.debian.org/perl6-team/modules/zef.git

This repo is now archived and cannot be modified

